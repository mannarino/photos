//
//  Image.swift
//  Photos
//
//  Created by Felipe Baptista on 17/03/20.
//  Copyright © 2020 Felipe Baptista. All rights reserved.
//

import UIKit

struct Image {
    
    let image: UIImage?
    var caption: String?
    var isShare: Bool = false
    
}
