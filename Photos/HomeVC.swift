//
//  ViewController.swift
//  Photos
//
//  Created by Felipe Baptista on 17/03/20.
//  Copyright © 2020 Felipe Baptista. All rights reserved.
//

import UIKit

class HomeVC: UICollectionViewController {

    
    var imageArray = [Image]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        // Do any additional setup after loading the view.
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        let button = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(touchUp))
        navigationItem.rightBarButtonItem = button
    }
    
    @objc func touchUp() {
        showImagePickerControllerActionSheet()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "cellID", for: indexPath)
        let imageView = cell.viewWithTag(1) as! UIImageView
        let label = cell.viewWithTag(2) as! UILabel
        let sharedIcon = cell.viewWithTag(3) as! UIImageView
        sharedIcon.isHidden = true
        imageView.image = imageArray[indexPath.row].image
        label.text = imageArray[indexPath.row].caption
        if(imageArray[indexPath.row].isShare){
            sharedIcon.isHidden = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return  UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(imageArray[indexPath.row].isShare)
        let editCaption = UIAlertAction(title: "Edit The Caption", style: .default) { (action) in
            
            //1. Create the alert controller.
            let alert = UIAlertController(title: "Comments", message: "Add more information to the photo", preferredStyle: .alert)

            let currentText = self.imageArray[indexPath.row].caption
            // 2 Create and add TextView to the alert
//            let margin:CGFloat = 10.0
//            let rect = CGRect(x: margin, y: margin, width: alert.view.bounds.size.width - margin * 4.0, height: 80.0)
//            let textView = UITextView(frame: rect)
//            if(currentText != nil) {
//                textView.text = currentText
//            }
//
//            alert.view.addSubview(textView)

            //2. Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                textField.placeholder = "Insert comment"
                if(currentText != nil) {
                    textField.text = currentText
                }
                
            }
            
            //3. Add Actions for the buttons
            let saveAction  = UIAlertAction(title: "Save", style: .default, handler: { (action) in
                let textField = alert.textFields![0]
                self.imageArray[indexPath.row].caption = textField.text
                collectionView.reloadData()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(saveAction)
            alert.addAction(cancelAction)

            // 4. Present the alert.
            self.present(alert, animated: true, completion: nil)
            
            
        }
        
        let deleteAction = UIAlertAction(title: "Delete This Photo", style: .destructive) { (action) in
            let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                self.imageArray.remove(at: indexPath.row)
                self.collectionView.deleteItems(at: [indexPath])
            }
            let cancelAction = UIAlertAction(title: "No", style: .destructive) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            
            AlertService.showAlert(style: .alert, title: "Delete image", message: "Are you sure you want to remove the photo?", actions: [okAction, cancelAction])
        }
        
        let shareAction = UIAlertAction(title: "Share This Photo", style: .default) { (action) in
            
            let okAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                self.imageArray[indexPath.row].isShare = true
                self.dismiss(animated: true, completion: nil)
                self.collectionView.reloadData()
            }
            let cancelAction = UIAlertAction(title: "No", style: .destructive) { (action) in
                self.imageArray[indexPath.row].isShare = false
                self.dismiss(animated: true, completion: nil)
                self.collectionView.reloadData()
            }
            
            AlertService.showAlert(style: .alert, title: "Share This Photo", message: "Would you like to share this evidence with the other inspectors?", actions: [okAction, cancelAction])
        }
        
        
        
        AlertService.showAlert(style: .actionSheet, title: nil, message: nil, actions: [editCaption, shareAction, deleteAction ], completion: nil)
    }

}

extension HomeVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showImagePickerControllerActionSheet() {
        
        let photoLibraryAction = UIAlertAction(title: "Choose From Library", style: .default) { (action) in
            self.showImagePickerController(sourceType: .photoLibrary)
        }
        
        let cameraAction = UIAlertAction(title: "Take a New Photo", style: .default) { (action) in
            self.showImagePickerController(sourceType: .camera)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        AlertService.showAlert(style: .actionSheet, title: "Choose your image", message: nil, actions: [photoLibraryAction, cameraAction, cancelAction], completion: nil)
    }
    
    func showImagePickerController(sourceType: UIImagePickerController.SourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        imagePickerController.sourceType = sourceType
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var image : Image = Image(image: nil, caption: nil, isShare: false)
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            image = Image(image: editedImage, caption: nil, isShare: false)
            
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = Image(image: originalImage, caption: nil, isShare: false)
        }
        
        let shareAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            image.isShare = true
            self.imageArray.append(image)
            self.dismiss(animated: true, completion: nil)
            self.collectionView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "No", style: .destructive) { (action) in
            image.isShare = false
            self.imageArray.append(image)
            self.dismiss(animated: true, completion: nil)
            self.collectionView.reloadData()
        }
        
        AlertService.showAlert(style: .actionSheet, title: "Share This Photo", message: "Would you like to share this evidence with the other inspectors?", actions: [shareAction, cancelAction])
        
        
    }
}

